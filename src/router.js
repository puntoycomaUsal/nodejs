'use strict';

/** DEPENDENCIAS */
const express                           = require('express');
const { fork }                          = require('child_process');
require('dotenv').config();
const calculatorPath                    = process.env.CALCULATOR_PATH;
const {getOperators}                    = require('./model_operator.js');
const {getOperations,getOperation,deleteOperation,updateOperation}      = require('./model_operation.js');

/** PROMISE */
const fifo  = function (req ) { return new Promise((resolve,reject) =>{
    //Usaremos esta promesa para gestionar la cola de solicitudes
    //de cálculo a través de las funciones  .then() para además
    //tener sincronización entre procesos. 
    
    //Traemos el proceso calculator que será un proceso hijo
    const calculator = fork(calculatorPath);
    try{
        calculator.send({
            '_id'       : req.body._id,
            'operator'  : req.body.operator,
            'op1'       : parseInt(req.body.op1),
            'op2'       : parseInt(req.body.op2),
        });
        //A la escucha de la respuesta.
        calculator.on('message', (msg) => {
            //Cerramos el proceso hijo, ya no nos sirve.
            calculator.kill();
            resolve (msg);
        });
    } catch (e) {
        reject (e.message);
    }
});};

/** ROUTER */
const router                            = express.Router();

/** GET */
router.get('/', (req, res) => {
    let data = {
        routes: {
            '/operation': {
                methods: ['GET']
            },
            '/operation/_id': {
                methods: ['GET']
            },
            '/operation/operator/op1/op2': {
                methods: ['POST']
            },
            '/operation/_id/operator/op1/op2': {
                methods: ['PUT']
            },
            '/operator': {
                methods: ['GET']
            },
            '/operation/_id': {
                methods: ['DELETE']
            },
            
        }
    }
    res.json(data);
});

router.get('/operation', (req, res) => {
        getOperations()
            .then(opertaions => {
                res.status(200)
                res.json(opertaions);
            })
            .catch( error => {
                res.status(500);
                res.send(error);
            });

});

router.get('/operation/:_id', (req, res) => {
    getOperation(req.params['_id'])
        .then((data) => {
            res.status(200);
            res.json(data);
        })
        .catch( error => {
            res.status(500);
            res.send({});
        });
});

router.get('/operator', (req, res) => {
        getOperators()
            .then(operators => {
                res.status(200)
                res.json(operators);
            })
            .catch( error => {
                res.status(500);
                res.send({});
            });

})

/** POST */
router.post('/operation', [checkParameters,getResult]);

/** PUT */
router.put('/operation', [checkParameters,getResult]);

/** DELETE */
router.delete('/operation', (req, res) => {
        deleteOperation(req.body._id)
            .then((data) => {
                res.status(200)
                res.json(data);
            })
            .catch( error => {
                res.status(500);
                res.send({});
            });

});

/** MIDDLEWARE */

function checkParameters (req, res,next) {
    // Comprobar los parámetros

    try{
        /**Hacemos la comprobación de tipos con un "cast", para que si 
        la variable no es entera, nos de un error.
        Dado que no es objeto de la práctica complicarse en exceso con la comprobación
        de tipos, simplemente usaremos la función que provee el sistema, si el usuario
        introduce caracteres o valores decimales, deberá comprobar la coherencia de
        sus valores de entrada respecto a su salida. */

        req.body.op1   = parseInt(req.body.op1);
        req.body.op2   = parseInt(req.body.op2);

        
        // Comprobamos que el operador se encuentre entre los elementos permitidos de la lista.
        getOperators()
            .then(operators => {

                if (operators.find((operators) => operators.operator === req.body.operator ) == undefined){
                    res.status(500);
                    res.send(`El operador ${req.params['operator']} no es un operador permitido.`);
                } else {
                    next();
                }
            })
            .catch( error => {
                res.status(500);
                res.send(error);
            });
    } catch (e) {
        // Si se produce cualquier error desconocido, lo devolveremos con un estado de 
        // error interno
        res.status(500);
        res.send(e.message);
    }
}

function getResult (req, res) {
    //Obtener los resultados
    fifo(req)
        .then(result => {
            //Todo correcto
            res.status(200);
            res.json(result);
        })
        .catch( error => {
            res.status(500);
            res.send(error);
        } );
}

module.exports          = router;
