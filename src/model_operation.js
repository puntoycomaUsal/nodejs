'use strict';
const mongoose = require('mongoose');

require('dotenv').config();

mongoose.connect(process.env.MONGODB_URL);
mongoose.Promise = global.Promise;

const OperationSchema = mongoose.Schema({
    operator: String,
    op1     : Number,
    op2     : Number,
    result  : Number
});

const Operation = mongoose.model('Operation', OperationSchema);

exports.getOperations = () => {
    return Operation.find({}).exec();
};

exports.getOperation = (_id) => {
    return Operation.findOne({_id: _id}).exec();
};

exports.deleteOperation = (_id) => {
    return Operation.remove({_id: _id});
};

exports.removeAllOperations = () => {
    return Operation.deleteMany();
};

exports.saveOperation = (operation) => {
    return (new Operation(operation)).save();
};

exports.updateOperation = (operation) => {
    return (new Operation(operation)).update({ 
        operator: operation.operator,
        op1     : operation.op1,
        op2     : operation.op2,
        result  : operation.result}); 
};
