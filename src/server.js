'use strict';
console.log ('Arrancanco servidor ............');
const express                           = require('express');
const bodyParser                        = require('body-parser');
const router                            = require('./router.js');
console.log ('Dependencias  ............... OK');

/** PROMISE */
const iniServer  = new Promise((resolve,reject) =>{

    try{
        const {removeAllOperators,saveOperators} = require('./model_operator.js');
        const {removeAllOperations}              = require('./model_operation.js');
        require('dotenv').config();
        const port              = process.env.PORT;
        const api_url           = process.env.API_URL;
        const operators         = process.env.OPERATORS;
        const app               = express();
        app.use(bodyParser.json());
        app.use(api_url, router);
        console.log ('Configuración Variables ... OK');

        //Operaciones previas a arranque para preparar base de datos
        removeAllOperators()
            .then(result => {
                console.log('Borrando operadores ... OK');
                removeAllOperations()
                    .then(() => {
                        console.log('Borrando operaciones ... OK');
                        saveOperators(JSON.parse(operators))
                            .then(() => {
                                console.log('Iniciando operadores ... OK');
                                app.listen(port);
                                console.log (`Servidor escuchando en puerto ${port}`);
                                resolve (app);
                            })
                            .catch( error => {
                                reject (error);
                            });
                    })
                    .catch( error => {
                        reject (error);
                    });
            })
            .catch( error => {
                reject (error);
            });
    } catch (e){
        reject(e.message);
    }
});

/** INIT */
iniServer
    .then((appInit) => {
        const app   = appInit;
        })
    .catch( error => {
        console.error(error);
        process.exit();
    });