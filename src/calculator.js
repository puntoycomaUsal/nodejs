'use strict';
const { saveOperation, updateOperation } = require('./model_operation.js');

process.on('message', (msg) => {
    //Si recibimos un mensaje, calculamos y devolvemos resultados
    let model = {}

    try {
        model ={
            "op1": msg.op1,
            "op2": msg.op2,
            "operator": msg.operator,
            "result": -1,
        };
        
        switch (msg.operator){
            case '+':
                //Suma
                model.result = msg.op1 + msg.op2;
            break;
            case '-':
                //Resta
                model.result = msg.op1 - msg.op2;
            break;
            case '*':
                model.result = msg.op1 * msg.op2;
            break;
            case '/':
                //División
                model.result = msg.op1 / msg.op2;
            break;
            default:
                //Para cualquier otra operación se devolverá cero.
                model.result = 0;
            break;
        }

        if (msg._id != undefined && msg._id.length != 0){
            model._id = msg._id
            updateOperation(model)
                .then (model => {
                    model.result    =  parseInt(model.result);
                    process.send(model);
                    })
                .catch (error => {
                    process.send({})
                });
        } else {
            saveOperation(model)
                .then (model => {
                    model.result    =  parseInt(model.result);
                    process.send(model);
                    })
                .catch (error => {
                    process.send(error)
                });
        }
    }catch (e){
        model.result = e.message;
        process.send(model);
    }
});