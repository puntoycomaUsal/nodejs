'use strict';
const mongoose = require('mongoose');

require('dotenv').config();

mongoose.connect(process.env.MONGODB_URL);
mongoose.Promise = global.Promise;

const OperatorSchema = mongoose.Schema({
    operator: String,
    description: String
});

const Operator = mongoose.model('Operator', OperatorSchema);

exports.getOperators = () => {
    return Operator.find({}).exec();
};

exports.removeAllOperators = () => {
    return Operator.deleteMany();
};

exports.saveOperators = (operators) => {
    return Operator.create(operators);
};
