//El cometido de este proceso es cada cierto tiempo (función timeout) descargar
// los tweets de la api de twitter seleccionados a través del filtro e 
// insertarlos en la base de datos MongoDB

'use strict';
// Objeto para gestinar la API de twitter.
const Twitter = require('twitter');
// Objeto que hemos creado para guardar en MongoBD los tweets
const { saveTweet } = require('./model.js');
// Traemos la configuración de variables.
require('dotenv').config();

//Creamos nuestro objeto de comunicación con la api de twitter.
const client = new Twitter({
  consumer_key: process.env.CONSUMER_KEY,
  consumer_secret: process.env.CONSUMER_SECRET,
  access_token_key: process.env.ACCESS_TOKEN_KEY,
  access_token_secret: process.env.ACCESS_TOKEN_SECRET
});

//Variable que usaremos para manejar los jsons
let stream = undefined;
//Variable que usaremos para conocer el estado del sub-proceso.
//Si nos fijamos, el proceso comienza en estado parado.
let status = 'STOP';

function onTweet(data) {
  //Callback que llamamos para guardar los datos en la base de datos MongoDB
  saveTweet(data).then(() => {
    //Es un bucle infinito que cada 1000 milisegundos (o segundos ver la doc de setTimeout())
    //Ejecuta el callback o función como parámetro, en este caso una función flecha. 
    //Parece por tanto que cada cierto tiempo almacena el contenido del buffer en la
    //Base de datos MongoDB,
    setTimeout(() => stream.once('data', onTweet), 1000);
  });
}

//Aquí es donde atendemos o escuchanos los mensajes del proceso padre
process.on('message', (msg) => {
  if (msg.cmd === 'START' && status === 'STOP') {
    //Si el padre nos manda arrancar y el estado es parado
    //cargamos en la variable stream, los tweets filtrados por la variable filter
    //que viene del padre. 
    //La documentación de la api de twitter se puede consultar en https://developer.twitter.com/en/docs
    stream = client.stream('statuses/filter', {track: msg.filter});
    //Ojo, usamos once, quiere decir que solo se ejecuta una vez, recordar que
    //con esto nos suscribimos al evento 'data' y si este se produce, llamamos al callback
    // onTweet que está más arriba, entiendo que se produce cuando se termina de recibir datos
    // al stream.
    stream.once('data', onTweet);
    //Cambiamos la variable de estado.
    status = msg.cmd;
  }
  if (msg.cmd === 'STOP' && status === 'START') {
    // Si el padre nos indica parar y el proceso está arrancado, paramos 
    // y destruimos el stream (el flujo de datos y dejamos el subproceso hijo parado
    stream.destroy();
    stream = undefined;
    status = msg.cmd;
  }
});