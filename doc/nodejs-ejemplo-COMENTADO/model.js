//Este fichero codifica las acciones propias del modelo de datos
//es el encargado de encapsular las operaciones CRUD.

'use strict';
//Traemos el objeto con el driver para conectarnos a la base de datos.
const mongoose = require('mongoose');

//De nuevo traemos la configuración del proyecto.
require('dotenv').config();

//Conectamos al servidor (local, sin usuario ni contraseña)
mongoose.connect(process.env.MONGODB_URL);
//Supongo que será para usar mongoose en modo "promesas"
mongoose.Promise = global.Promise;
//Creamos el esquema con el que va a trabajar nuestra colección llamada Tweet
const TweetSchema = mongoose.Schema({
  created_at: String,
  id: String,
  text: String,
});
const Tweet = mongoose.model('Tweet', TweetSchema);

//Operaciones sobre la colección tweet, creo que no necesita mucha explicación. 

exports.getTweets = () => {
  return Tweet.find({}).exec();
};

exports.getTweet = (id) => {
  return Tweet.findOne({id: id}).exec();
};

exports.getTweetsByFilter = (filter) => {
  return Tweet.findOne(filter).exec();
};

exports.saveTweet = (tweet) => {
  return (new Tweet(tweet)).save();
};
