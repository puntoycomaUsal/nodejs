//Este fichero es donde se agrupan las operaciones del servidor
//que puede realizar nuestra api. 

'use strict';
const express = require('express');
//Carga de operaciones del modelo
const { getTweets, getTweet, getTweetsByFilter } = require('./model.js');
//Creamos un objeto Router
const router = express.Router();

//Si se recibe una solictud contra la raiz de la API
//devuelve un objeto json con los endponts y los métodos de la API
router.get('/', (req, res) => {
  let data = {
    routes: {
      '/tweets': {
        methods: ['GET', 'POST']
      },
      '/tweets/find': {
        methods: ['POST']
      }
    }
  }
  res.json(data);
});

//Traemos los tweets de la base de datos MongoDB
router.get('/tweets', (req, res) => {
  getTweets().then((data) => {
    res.json(data);
  });
});

//Traemos un tweet por su valor, por su id.
router.get('/tweets/:id', (req, res) => {
  let id = req.params.id;
  getTweet(id).then((data) => {
    res.json(data);
  });
});

//Nos hacen una petición POST para enviar un mensaje al proceso, entiendo que 
//para  empezar o parar la recolección de tweets.
router.post('/tweets', (req, res) => {
  let data = req.body;
  req.app.get('child').send(data);
  res.json(data);
});

//Nos permite buscar un tweet
router.post('/tweets/find', (req, res) => {
  let filter = req.body;
  getTweetsByFilter(filter).then((data) => {
    res.json(data);
  });
});

module.exports = router;
