//Es el punto de entrada del programa, con $ node server.js se arranca
//Lo primero hay que ejecutar, (antes que la orden anterior) $ npm install
//para que instale los paquetes de package,json
// Se usa el modo estricto es decir se hace control de tipos, etc...
'use strict';

//Se carga el objeto child_process para poder crear procesos hijos
const { fork } = require('child_process');
// Carga del framework express para crear servidores.http
const express = require('express');
// carga de un "parser", entiendo que para manejar JSON
const bodyParser = require('body-parser');
// Carga del módulo de rutas del servidor.
const router = require('./router.js');
// Trae el fichero .env que es donde se han configurado las cadenas de variables
require('dotenv').config();

// Creación de constantes, recordar, const si no cambia la variable en todo su ámbito, let si carga
const port = process.env.PORT;
// El proceso hijo
const childUrl = 'process.js';
//Creamos en app un objeto de tipo express. 
const app = express();
//Creamos el proceso hijo que funcionará de forma asíncrona. 
const child = fork(childUrl);

// Con .set() lo que hacemos es asignar una variable (propiedad) nueva al objeto app (el objeto express)
app.set('child', child);
// .use() se usa para añadir middleware aquí se explica mejor https://stackoverflow.com/questions/11321635/nodejs-express-what-is-app-use
app.use(bodyParser.json());
// En este caso añadde a la ruta del server (localhost:8080/+/api/v1/) para acceder a su url.
app.use('/api/v1/', router);
//Enoezanos a escuhar peticiones.
app.listen(port);

//Añadimos ahora una solicitud para traer los tweets del proceso hijo. Para ello vamos a 
//enviarle un mensaje con 2 parámetros, uno para que arranque y otro para que en este caso
//filtre los mensajes que trae desde twitter, si el filtro está vacío da un error, (se puede)
//mejorar para que si filter está vacío ejecute otra api)
child.send({
    'cmd': 'START',
    'filter':'Ubuntu'
});